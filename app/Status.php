<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB; //DB belongs to this dictionary - allows to get other tables

class Status extends Model
{
    public function candidates(){
        return $this->hasMany('app\Candidate');
    }

    //input - existing status -- output - approved statuses
    public static function next($status_id){
        // SELECT to FROM nextstages WHERE from=status_id
        $nextstages = DB::table('nextstages')->where('from',$status_id)->pluck('to');
        //after we recieved the answer (exp: 1->2,3) / we want to return the STATUSES rows (id and names..)
        //we use static function(self::find) because we ask the [status CLASS] not the [function next].
        return self::find($nextstages)->all();
    }

    public static function allowed($from,$to){
        $allowed = DB::table('nextstages')->where('from',$from)->where('to',$to)->get();
        if(isset($allowed)) return true;
        return false;
    }
}
