<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $fillable = ['name','email']; // allowed these fields mass assignment


    public function owner(){
        return $this->belongsTo('App\User','user_id') ;
    }

    public function status(){
        return $this->belongsTo('App\Status','status_id') ;
    }

}