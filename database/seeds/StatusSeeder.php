<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            'name' => 'Before Interview',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('statuses')->insert([
            'name' => 'Not Fit',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('statuses')->insert([
            'name' => 'Sent To Manager',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('statuses')->insert([
            'name' => 'Not Fit Professionally',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
