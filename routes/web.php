<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// middleware('auth') - middleware check if the user is authenticated
Route::resource('candidates', 'CandidatesController')->middleware('auth');

// create route for DELETE method using GET (href work only with GET)
//1. URI  2. action   3. name - for simple work when the URI will change
Route::get('candidates/delete/{id}','CandidatesController@destroy')->name('candidate.delete');

Route::get('candidates/changeuser/{cid}/{uid?}','CandidatesController@changeUser')->name('candidate.changeuser');

Route::get('candidates/changestatus/{cid}/{sid}','CandidatesController@changeStatus')->name('candidate.changestatus')->middleware('auth');

Route::get('mycandidates','CandidatesController@myCandidates')->name('candidates.mycandidates')->middleware('auth');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');