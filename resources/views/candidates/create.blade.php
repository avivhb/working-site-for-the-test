@extends('layouts.app')

@section('title','Create Candidate')

@section('content');

    <h1>Create Candidate</h1>
    <!-- php artisan route:list -->
    <form method = "post" action ="{{action('CandidatesController@store')}}">
        @csrf <!-- Security -->
        <div class="form-group">
            <label for = "name">Candidate Name</label>
            <input type = "text" class="form-control" name = "name"> <!-- lable for = "X" , name = "X" -->
        </div>
        <div class="form-group">
            <label for = "email">Candidate Email</label>
            <input type = "text" class="form-control" name = "email"> 
        </div>
        <div>
            <input type = "submit" name = "submit" value = "Create Candidate">
        </div>
    </form>
@endsection;